from django.apps import AppConfig


class Lab11AppConfig(AppConfig):
    name = 'Lab11_App'
